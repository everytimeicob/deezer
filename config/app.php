<?php

return [

    'env' => defined('APP_ENV') ? APP_ENV : 'production',
    'url' => defined('APP_URL') ? APP_URL : 'http://localhost',
    'timezone' => 'Europe/Paris',
    'locale' => 'fr',
    'fallback_locale' => 'en',
    'providers' => [
        App\Providers\AppServiceProvider::class,
        App\Providers\EntityManagerProvider::class,
        App\Providers\RouteServiceProvider::class,
    ],
];
