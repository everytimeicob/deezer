<?php

return [
    'default' => defined('DB_CONNECTION') ? DB_CONNECTION : 'mysql',
    'mysql' => [
        'use_socket' => defined('DB_SOCKET') && !empty(DB_SOCKET) ? true : false,
        'host' => defined('DB_HOST') ? DB_HOST : '127.0.0.1',
        'socket' => defined('DB_SOCKET') ? DB_SOCKET : '',
        'port' => defined('DB_PORT') ? DB_PORT : '3306',
        'database' => defined('DB_DATABASE') ? DB_DATABASE : 'forge',
        'username' => defined('DB_USERNAME') ? DB_USERNAME : 'forge',
        'password' => defined('DB_PASSWORD') ? DB_PASSWORD : '',
        'options' => [
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ],
    ],
];
