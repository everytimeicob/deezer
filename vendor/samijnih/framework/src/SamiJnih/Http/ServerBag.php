<?php

namespace SamiJnih\Http;

class ServerBag extends ParameterBag
{
    /**
     * Returns the HTTP headers.
     *
     * @return array
     */
    public function getHeaders() : array
    {
        $headers = [];
        $contentHeaders = ['CONTENT_LENGTH', 'CONTENT_TYPE'];

        foreach ($this->bag as $key => $value) {
            if (strpos($key, 'HTTP_') === 0) {
                $headers[substr($key, 5)] = $value;
            } elseif (in_array($key, $contentHeaders)) {
                $headers[$key] = $value;
            }
        }

        return $headers;
    }
}
