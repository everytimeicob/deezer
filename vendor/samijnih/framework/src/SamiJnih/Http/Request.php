<?php

namespace SamiJnih\Http;

class Request
{
    const METHOD_HEAD = 'HEAD';
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';
    const METHOD_OPTIONS = 'OPTIONS';

    /**
     * @var ParameterBag
     */
    public $query;

    /**
     * @var ParameterBag
     */
    public $request;

    /**
     * @var ParameterBag
     */
    public $attributes;

    /**
     * @var ParameterBag
     */
    public $cookies;

    /**
     * @var array
     */
    public $files;

    /**
     * @var ServerBag
     */
    public $server;

    /**
     * @var HeaderBag
     */
    public $headers;

    /**
     * The request's method.
     *
     * @var string
     */
    protected $method;


    /**
     * Constructor.
     *
     * @param  array $query
     * @param  array $request
     * @param  array $attributes
     * @param  array $cookies
     * @param  array $files
     * @param  array $server
     *
     * @return void
     */
    public function __construct
    (
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = []
    )
    {
        $this->query = new ParameterBag($query);
        $this->request = new ParameterBag($request);
        $this->attributes = new ParameterBag(array_merge(['parameters' => new ParameterBag(), $attributes]));
        $this->cookies = new ParameterBag($cookies);
        $this->files = $files;
        $this->server = new ServerBag($server);
        $this->headers = new HeaderBag($this->server->getHeaders());
    }

    /**
     * Retrieves the full uri.
     *
     * @return string
     */
    public function getUri() : string
    {
        return $this->getSchemeAndHttpHost().$this->getPathInfo().$this->getQueryString(true);
    }

    /**
     * Retrieves the full url without query string.
     *
     * @return string
     */
    public function getUrl() : string
    {
        return rtrim(preg_replace('/\?.*/', '', $this->getUri()), '/');
    }

    /**
     * Retrieves the request's scheme.
     *
     * @return string
     */
    public function getScheme() : string
    {
        return $this->server->get('REQUEST_SCHEME');
    }

    /**
     * Retrieves the request's http host.
     *
     * @return string
     */
    public function getHttpHost() : string
    {
        return $this->headers->get('HOST');
    }

    /**
     * Joins the request's scheme and http host.
     *
     * @return string
     */
    public function getSchemeAndHttpHost() : string
    {
        return $this->getScheme().'://'.$this->getHttpHost();
    }

    /**
     * Retrieves the request's path info.
     *
     * @return string
     */
    public function getPathInfo() : string
    {
        return rtrim(preg_replace('/\?.*/', '', $this->server->get('REQUEST_URI')), '/');
    }

    /**
     * Retrieves the request's query string.
     *
     * @param  bool $mark Defines if it should return the query string with the question mark.
     *
     * @return string
     */
    public function getQueryString($mark = false) : string
    {
        return ($mark ? '?' : '').$this->server->get('QUERY_STRING');
    }

    /**
     * Retrieves the current request's method.
     *
     * @return string
     */
    public function getMethod() : string
    {
        if (is_null($this->method)) {
            $this->method = strtoupper($this->server->get('REQUEST_METHOD', 'GET'));

            if ($this->method === 'POST') {
                if ($this->request->has('_method')) {
                    $this->method = strtoupper($this->request->get('_method', 'POST'));
                }
            }
        }

        return $this->method;
    }
}
