<?php

namespace SamiJnih\Http;

class ParameterBag implements \Countable, \IteratorAggregate
{
    /**
     * The storage of parameters.
     *
     * @var array
     */
    protected $bag = [];


    /**
     * Constructor.
     *
     * @param  array|[] $parameters An array containing parameters to store.
     *
     * @return void
     */
    public function __construct(array $parameters = [])
    {
        $this->bag = $parameters;
    }

    /**
     * Adds new values to the bag.
     *
     * @param  array $values
     *
     * @return void
     */
    public function add(array $values)
    {
        $this->bag = array_replace($this->bag, $values);
    }

    /**
     * Sets a value to the given key.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return void
     */
    public function set(string $key, $value)
    {
        $this->bag[$key] = $value;
    }

    /**
     * Gets a value from the bag with a default value.
     *
     * @param  string $key
     * @param  mixed  $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        return $this->bag[$key] ?? $default;
    }

    /**
     * Checks if the given key exists from the bag.
     *
     * @param  string $key
     *
     * @return bool
     */
    public function has(string $key) : bool
    {
        return isset($this->bag[$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->bag);
    }

    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
        return count($this->bag);
    }
}
