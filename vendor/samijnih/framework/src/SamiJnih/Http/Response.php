<?php

namespace SamiJnih\Http;

class Response
{
    /**
     * The response's headers.
     *
     * @var HeaderBag
     */
    public $headers = [];

    /**
     * The response's content.
     *
     * @var string
     */
    protected $content;

    /**
     * The status code.
     *
     * @var int
     */
    protected $statusCode;

    /**
     * The status text.
     *
     * @var string
     */
    protected $statusText;

    /**
     * The status texts.
     *
     * @var array
     */
    public $statusTexts = [
        200 => 'OK',
        201 => 'Created',
        204 => 'No Content',
        301 => 'Moved Permanently',
        304 => 'Not Modified',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        500 => 'Internal Server Error',
    ];


    /**
     * Constructor.
     *
     * @param  string $content
     * @param  int    $statusCode
     * @param  array  $headers
     *
     * @return void
     */
    public function __construct($content = '', $statusCode = 200, $headers = [])
    {
        $this->headers = new HeaderBag($headers);
        $this->setContent($content);
        $this->setStatusCode($statusCode);
    }

    /**
     * Sends the response's headers and content.
     *
     * @return Response
     */
    public function send() : Response
    {
        $this->sendHeaders();
        $this->sendContent();

        return $this;
    }

    /**
     * Sends the headers.
     *
     * @return Response
     */
    public function sendHeaders() : Response
    {
        if (headers_sent()) {
            return $this;
        }

        foreach ($this->headers as $headerKey => $headerValue) {
            foreach ((array) $headerValue as $subHeaderValue) {
                header($headerKey.': '.$subHeaderValue, false, $this->statusCode);
            }
        }

        header('HTTP/1.0 '.$this->getStatusCode().' '.$this->statusText, true, $this->statusCode);

        return $this;
    }

    /**
     * Sends the current content.
     *
     * @return Response
     */
    public function sendContent() : Response
    {
        echo $this->content;

        return $this;
    }

    /**
     * Sets the content.
     *
     * @param mixed $content
     */
    public function setContent($content) : Response
    {
        if (!is_null($content) && !is_string($content) && !is_numeric($content) && !is_callable([$content, '__toString'])) {
            throw new \UnexpectedValueException('The content of the response must be a string or a callable object implementing __toString().');
        }

        $this->content = (string) $content;

        return $this;
    }

    /**
     * Sets the status code.
     *
     * @param int $statusCode
     *
     * @return Response
     */
    public function setStatusCode($statusCode = 200) : Response
    {
        $this->statusCode = (int) $statusCode;
        $this->statusText = $this->statusTexts[$this->statusCode];

        return $this;
    }

    /**
     * Gets the current status code.
     *
     * @return int
     */
    public function getStatusCode() : int
    {
        return $this->statusCode;
    }

    /**
     * Gets the current status text.
     *
     * @return string
     */
    public function getStatusText() : string
    {
        return $this->statusText;
    }
}
