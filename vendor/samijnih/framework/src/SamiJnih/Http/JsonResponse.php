<?php

namespace SamiJnih\Http;

class JsonResponse extends Response
{
    /**
     * The json data.
     *
     * @var string
     */
    protected $data;


    /**
     * Constructor.
     *
     * @param  mixed $data
     * @param  int   $status
     * @param  array $headers
     *
     * @return void
     */
    public function __construct($data = [], $status = 200, $headers = [])
    {
        parent::__construct('', $status, $headers);

        if ($data === null) {
            $data = [];
        } elseif (!is_array($data)) {
            $data = (array) $data;
        }

        $this->setJson($data);
    }

    /**
     * Sets the json data to the response.
     *
     * @param  array $data
     *
     * @return Response
     */
    public function setJson(array $data) : Response
    {
        $this->data = json_encode($data, 15);

        if (!$this->headers->has('Content-Type') || ($this->headers->get('Content-Type') !== 'application/json')) {
            $this->headers->set('Content-Type', 'application/json');
        }

        return $this->setContent($this->data);
    }
}
