<?php

if (!function_exists('app')) {

    /**
     * Returns the current app instance.
     *
     * @return \SamiJnih\Container\Container
     */
    function app() : \SamiJnih\Container\Container
    {
        return SamiJnih\Container\Container::getInstance();
    }
}

if (!function_exists('base_path')) {

    /**
     * Returns the path to the root of the application.
     *
     * @param  string $path
     *
     * @return string
     */
    function base_path(string $path = '') : string
    {
        return app()->basePath().($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('app_path')) {

    /**
     * Returns the path to the app folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function app_path(string $path = '') : string
    {
        return base_path(app()->appPath().($path ? DIRECTORY_SEPARATOR.$path : $path));
    }
}

if (!function_exists('config_path')) {

    /**
     * Returns the path to the config folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function config_path(string $path = '') : string
    {
        return base_path(app()->configPath().($path ? DIRECTORY_SEPARATOR.$path : $path));
    }
}

if (!function_exists('database_path')) {

    /**
     * Returns the path to the database folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function database_path(string $path = '') : string
    {
        return base_path(app()->databasePath().($path ? DIRECTORY_SEPARATOR.$path : $path));
    }
}

if (!function_exists('storage_path')) {

    /**
     * Returns the path to the storage folder.
     *
     * @param  string $path
     *
     * @return string
     */
    function storage_path(string $path = '') : string
    {
        return base_path(app()->storagePath().($path ? DIRECTORY_SEPARATOR.$path : $path));
    }
}

if (!function_exists('snake_case')) {

    /**
     * Converts a string to snake_case.
     *
     * @param  string $string
     *
     * @return string
     */
    function snake_case(string $string) : string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);

        $string = $matches[0];

        foreach ($string as &$match) {
            $match = ($match == strtoupper($match)) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $string);
    }
}
