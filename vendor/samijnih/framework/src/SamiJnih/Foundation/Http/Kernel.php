<?php

namespace SamiJnih\Foundation\Http;

use SamiJnih\Contracts\Http\HttpKernelContract;
use SamiJnih\Contracts\Http\KernelContract;
use SamiJnih\Http\Request;

class Kernel implements KernelContract
{
    /**
     * An instance of the HttpKernelContract.
     *
     * @var HttpKernelContract
     */
    protected $httpKernel;


    /**
     * Constructor.
     *
     * @param  HttpKernelContract $httpKernel
     *
     * @return void
     */
    public function __construct(HttpKernelContract $httpKernel)
    {
        $this->httpKernel = $httpKernel;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request)
    {
        try {
            return $this->httpKernel->handle($request);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
