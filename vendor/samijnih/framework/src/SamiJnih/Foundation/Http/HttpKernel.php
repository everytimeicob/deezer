<?php

namespace SamiJnih\Foundation\Http;

use SamiJnih\Contracts\Http\HttpKernelContract;
use SamiJnih\Contracts\Resolver\ArgumentResolverContract;
use SamiJnih\Contracts\Resolver\ControllerResolverContract;
use SamiJnih\Http\Request;
use SamiJnih\Http\Response;

class HttpKernel implements HttpKernelContract
{
    /**
     * An instance of the ControllerResolverContract.
     *
     * @var ControllerResolverContract
     */
    protected $controllerResolver;

    /**
     * An instance of the ArgumentResolverContract.
     *
     * @var ArgumentResolverContract
     */
    protected $argumentResolver;


    /**
     * Constructor.
     *
     * @param  ControllerResolverContract $controllerResolver
     * @param  ArgumentResolverContract   $argumentResolver
     *
     * @return void
     */
    public function __construct
    (
        ControllerResolverContract $controllerResolver,
        ArgumentResolverContract $argumentResolver
    )
    {
        $this->controllerResolver = $controllerResolver;
        $this->argumentResolver = $argumentResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request)
    {
        try {
            $controller = $this->controllerResolver->getController($request);
            $arguments = $this->argumentResolver->getArguments($request, $controller);
            $response = call_user_func_array($controller, $arguments);

            if (!($response instanceof Response)) {
                throw new \LogicException('The controller must return an instance of Response');
            }

            return $response;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
