<?php

namespace SamiJnih\Foundation;

use SamiJnih\Container\Container;
use SamiJnih\Contracts\Provider\ServiceProviderContract;
use SamiJnih\Database\DB;
use SamiJnih\Exception\ClassNotFound;
use SamiJnih\Foundation\Http\HttpKernel;
use SamiJnih\Foundation\Http\Kernel;
use SamiJnih\Http\Request;
use SamiJnih\Resolver\ArgumentResolver;
use SamiJnih\Resolver\ControllerResolver;
use SamiJnih\Routing\Router;

class Application extends Container
{
    /**
     * The version of the framework.
     */
    const VERSION = '1.0.0';

    /**
     * Indicates the root path of the application.
     *
     * @var string
     */
    protected $basePath;

    /**
     * Indicates the app path of the application.
     *
     * @var string
     */
    protected $appPath = 'app';

    /**
     * Indicates the config path of the application.
     *
     * @var string
     */
    protected $configPath = 'config';

    /**
     * Indicates the database path of the application.
     *
     * @var string
     */
    protected $databasePath = 'database';

    /**
     * Indicates the storage path of the application.
     *
     * @var string
     */
    protected $storagePath = 'storage';

    /**
     * Indicates if the application has "booted".
     *
     * @var bool
     */
    protected $booted = false;

    /**
     * Indicates if the application has registered all the service providers.
     *
     * @var bool
     */
    protected $areServiceProvidersRegistered = false;

    /**
     * Contains all the loaded service providers.
     *
     * @var array
     */
    protected $loadedServiceProviders = [];


    /**
     * {@inheritdoc}
     * Initializes the application.
     *
     * @param  string $basePath The base path of the application.
     */
    public function __construct(string $basePath, array $values = [])
    {
        parent::__construct($values);

        $this->basePath = rtrim($basePath, '\/');

        $this->configurations();
        $this->register();
        $this->boot();
        $this->run();

    }

    /**
     * Returns the base path.
     *
     * @return string;
     */
    public function basePath()
    {
        return $this->basePath;
    }

    /**
     * Returns the app path.
     *
     * @return string;
     */
    public function appPath()
    {
        return $this->appPath;
    }

    /**
     * Returns the config path.
     *
     * @return string;
     */
    public function configPath()
    {
        return $this->configPath;
    }

    /**
     * Returns the database path.
     *
     * @return string;
     */
    public function databasePath()
    {
        return $this->databasePath;
    }

    /**
     * Returns the storage path.
     *
     * @return string;
     */
    public function storagePath()
    {
        return $this->storagePath;
    }

    /**
     * Runs the magic.
     */
    public function run()
    {
       $response = $this['kernel']->handle($this['router']->dispatch($this['request']));

       $response->send();
    }

    /**
     * Loads all the configurations.
     *
     * @return void
     */
    protected function configurations()
    {
        $configPath = config_path();
        $appConf = $configPath.DIRECTORY_SEPARATOR.'app.php';
        $databaseConf = $configPath.DIRECTORY_SEPARATOR.'database.php';

        if (file_exists($appConf)) {
            $this->config['app'] = require_once $appConf;
        }

        if (file_exists($databaseConf)) {
            $this->config['database'] = require_once $databaseConf;
        }
    }

    /**
     * Registers all the application's service providers.
     *
     * @return void
     */
    protected function register()
    {
        $this->registerCoreServiceProviders();
        $this->registerAdditionalServiceProviders();

        $this->areServiceProvidersRegistered = true;
    }

    /**
     * Registers all the core service providers.
     *
     * @return void
     */
    protected function registerCoreServiceProviders()
    {
        $this['db'] = new DB($this->config['database'] ?? null);
        $this['request'] = new Request($_GET, $_POST, [], $_COOKIE, $_FILES, $_SERVER);
        $this['router'] = new Router();
        $this['http_kernel'] = new HttpKernel(new ControllerResolver(), new ArgumentResolver($this));
        $this['kernel'] = new Kernel($this['http_kernel']);
    }

    /**
     * Registers all the additional service providers.
     *
     * @return void
     */
    protected function registerAdditionalServiceProviders()
    {
        if (!empty($this->config['app']['providers'])) {
            foreach ($this->config['app']['providers'] as $p) {
                if (class_exists($p)) {
                    $this->registerServiceProvider(new $p());
                } else {
                    throw new ClassNotFound($p);
                }
            }
        }
    }

    /**
     * Registers a given service provider.
     *
     * @param  ServiceProviderContract $serviceProvider
     *
     * @return void
     */
    protected function registerServiceProvider(ServiceProviderContract $serviceProvider)
    {
        if (in_array(get_class($serviceProvider), $this->loadedServiceProviders)) {
            return;
        }

        if (method_exists($serviceProvider, 'register')) {
            $serviceProvider->register($this);
        }

        $this->markAsRegistered($serviceProvider);
    }

    /**
     * Loads all the service providers' events.
     *
     * @return void
     */
    protected function boot()
    {
        if ($this->booted || !$this->areServiceProvidersRegistered) {
            return;
        }

        foreach ($this as $serviceProvider) {
            if (method_exists($serviceProvider, 'boot')) {
                $serviceProvider->boot($this);
            }
        }

        $this->booted = true;
    }

    /**
     * Marks a service provider as registered.
     *
     * @param  ServiceProviderContract $serviceProvider
     *
     * @return void
     */
    protected function markAsRegistered(ServiceProviderContract $serviceProvider)
    {
        $this->loadedServiceProviders[] = get_class($serviceProvider);
    }
}
