<?php

namespace SamiJnih\Contracts\Provider;

use SamiJnih\Foundation\Application;

interface ServiceProviderContract
{
    /**
     * Registers new events in the app.
     *
     * @param  Application $app
     *
     * @return void
     */
    public function boot(Application $app);

    /**
     * Registers new services in the app.
     *
     * @param  Application $app
     *
     * @return void
     */
    public function register(Application $app);
}
