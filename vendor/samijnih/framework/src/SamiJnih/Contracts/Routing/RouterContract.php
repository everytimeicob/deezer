<?php

namespace SamiJnih\Contracts\Routing;

interface RouterContract
{
    /**
     * Registers a new GET route.
     *
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function get($uri, $handler);

    /**
     * Registers a new POST route.
     *
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function post($uri, $handler);

    /**
     * Registers a new PUT route.
     *
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function put($uri, $handler);

    /**
     * Registers a new PATCH route.
     *
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function patch($uri, $handler);

    /**
     * Registers a new DELETE route.
     *
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function delete($uri, $handler);
}
