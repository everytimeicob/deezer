<?php

namespace SamiJnih\Contracts\Http;

use SamiJnih\Http\Request;

interface KernelContract
{
    /**
     * Handles the request.
     *
     * @param  Request $request
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function handle(Request $request);
}
