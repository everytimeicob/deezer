<?php

namespace SamiJnih\Database\Driver;

use PDO;
use RuntimeException;

class Mysql extends PDO
{
    /**
     * Constructor.
     *
     * @param  array $config
     *
     * @return void
     */
    public function __construct(array $config = [])
    {
        if (empty($config['host']) && ($config['use_socket'] == false)) {
            throw new RuntimeException("You must define the host of your database connection.");
        } elseif (empty($config['socket']) && ($config['use_socket'] == true)) {
            throw new RuntimeException("You must define the unix_socket of your database connection.");
        } elseif (empty($config['port'])) {
            throw new RuntimeException("You must define the port of your database connection.");
        } elseif (empty($config['database'])) {
            throw new RuntimeException("You must define the database of your database connection.");
        } elseif (empty($config['username'])) {
            throw new RuntimeException("You must define the username of your database connection.");
        } elseif (!isset($config['password'])) {
            throw new RuntimeException("You must define the password of your database connection.");
        }

        $config['options']['charset'] = $config['options']['charset'] ?? 'utf8';

        parent::__construct($this->getDns($config), $config['username'], $config['password']);
    }

    /**
     * Retrieves the good configuration.
     *
     * @param  array $config
     *
     * @return string
     */
    protected function getDns(array $config) : string
    {
        return $this->hasSocket($config) ? $this->getSocketDns($config) : $this->getHostDns($config);
    }

    /**
     * Checks if the configuration define a socket connection.
     *
     * @param  array $config
     *
     * @return bool
     */
    protected function hasSocket(array $config) : bool
    {
        return !empty($config['use_socket']);
    }

    /**
     * Retrieves the socket dns.
     *
     * @param  array $config
     *
     * @return string
     */
    protected function getSocketDns(array $config) : string
    {
        return "mysql:unix_socket={$config['socket']};dbname={$config['database']};charset={$config['options']['charset']}";
    }

    /**
     * Retrieves the host dns.
     *
     * @param  array $config
     *
     * @return string
     */
    protected function getHostDns(array $config) : string
    {
        return "mysql:host={$config['host']};port={$config['port']};dbname={$config['database']};charset={$config['options']['charset']}";
    }
}
