<?php

namespace SamiJnih\Database;

class DB
{
    /**
     * Constructor.
     *
     * @param  array $config
     *
     * @return void
     */
    public function __construct(array $config = [])
    {
        if (empty($driverName = $config['default'])) {
            throw new \RuntimeException("You must define a default driver for the database connection.");
        } elseif (empty($config[$driverName])) {
            throw new \RuntimeException("You must define a key named '{$driverName}'.");
        }

        $driver = 'SamiJnih\\Database\\Driver\\'.ucfirst($driverName);

        if (class_exists($driver)) {
            $this->driver = new $driver($config[$driverName]);
        } else {
            throw new \RuntimeException("The requested driver does not exist.");
        }
    }

    /**
     * Calls directly methods on the driver.
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->driver, $method], $arguments);
    }
}