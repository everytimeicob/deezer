<?php

namespace SamiJnih\Routing;

use SamiJnih\Contracts\Routing\RouterContract;
use SamiJnih\Http\Exception\RouteNotFound;
use SamiJnih\Http\Request;

class Router implements RouterContract
{
    /**
     * An instance of the RouteCollection.
     *
     * @var RouteCollection
     */
    protected $routes;

    /**
     * The current matched route.
     *
     * @var Route
     */
    protected $currentRoute;


    /**
     * Constructor.
     *
     * @param  RouteCollection $routeCollection
     *
     * @return void
     */
    public function __construct()
    {
        $this->routes = new RouteCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function get($uri, $handler)
    {
        $this->routes->addRoute(['GET', 'HEAD'], $uri, $handler);
    }

    /**
     * {@inheritdoc}
     */
    public function post($uri, $handler)
    {
        $this->routes->addRoute('POST', $uri, $handler);
    }

    /**
     * {@inheritdoc}
     */
    public function put($uri, $handler)
    {
        $this->routes->addRoute('PUT', $uri, $handler);
    }

    /**
     * {@inheritdoc}
     */
    public function patch($uri, $handler)
    {
        $this->routes->addRoute('PATCH', $uri, $handler);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($uri, $handler)
    {
        $this->routes->addRoute('DELETE', $uri, $handler);
    }

    /**
     * Dispatches the request.
     *
     * @param  Request $request
     *
     * @throws RouteNotFound Thown if the current request does not match any route.
     *
     * @return Request
     */
    public function dispatch(Request $request) : Request
    {
        if (null === ($request = $this->findRoute($request))) {
            throw new RouteNotFound();
        }

        return $request;
    }

    /**
     * Checks if the current request matches a route from the route collection.
     *
     * @param  Request $request
     *
     * @return Request|null
     */
    protected function findRoute($request)
    {
        $uri = $request->getPathInfo();

        foreach ($this->routes as $route) {
            if (
                preg_match($this->getRouteRegex($route), $uri)
                && in_array($request->getMethod(), $route->getMethod())
            ) {
                $this->currentRoute = $route;

                $request->attributes->set('route', $route);
                $request->attributes->get('parameters')->add($this->getRouteParameters($request, $route));

                return $request;
            }
        }

        return null;
    }

    /**
     * Retrieves all the parameters of a given route.
     *
     * @param  Request $request
     * @param  Route   $route
     *
     * @return array
     */
    protected function getRouteParameters(Request $request, Route $route) : array
    {
        preg_match_all('/\{([a-z0-9_]+)\}/i', $route->getUri(), $keys);
        preg_match($this->getRouteRegex($route), $request->getPathInfo(), $values);

        if (isset($keys[1])) {
            $keys = array_unique($keys[1]);
            $values = array_slice($values, 1);

            if (count($values) === count($keys)) {
                return array_combine($keys, $values);
            } else {
                throw new \InvalidArgumentException('The given parameters to the route '.$route->getUri().' cannot be the same.');
            }
        } else {
            return [];
        }
    }

    /**
     * Retrieves the regexp of a given route.
     *
     * @param  Route $route
     *
     * @return string
     */
    protected function getRouteRegex(Route $route) : string
    {
        return '@^'.preg_replace(
            ['/\//', '/\{[a-zA-Z0-9_]+\}/'],
            ['\\/', '([^/])'],
            $route->getUri()
        ).'$@';
    }

    /**
     * Returns the current matched route.
     *
     * @return Route|null
     */
    public function getCurrentRoute()
    {
        return $this->currentRoute;
    }
}
