<?php

namespace SamiJnih\Routing;

use SamiJnih\Http\Request;

class RouteCollection implements \IteratorAggregate
{
    /**
     * The routes collection.
     *
     * @var array
     */
    protected $routes = [];


    /**
     * Adds a route to the route collection.
     *
     * @param  array|string    $method
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function addRoute($method, $uri, $handler)
    {
        $this->routes[] = new Route($method, $uri, $handler);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator() : \ArrayIterator
    {
        return new \ArrayIterator($this->routes);
    }
}
