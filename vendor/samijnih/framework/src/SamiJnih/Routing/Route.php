<?php

namespace SamiJnih\Routing;

class Route
{
    /**
     * The route's methods.
     *
     * @var array
     */
    protected $method;

    /**
     * The route's uri.
     *
     * @var string
     */
    protected $uri;

    /**
     * The route's handler.
     *
     * @var \Closure|string
     */
    protected $handler;


    /**
     * Constructor.
     *
     * @param  array|string    $method
     * @param  string          $uri
     * @param  \Closure|string $handler
     *
     * @return void
     */
    public function __construct($method, string $uri, $handler)
    {
        $this->method = array_map('strtoupper', (array) $method);
        $this->uri = $uri;
        $this->handler = $handler;
    }

    /**
     * Retrieves the handler's type.
     *
     * @return string
     */
    public function getHandlerType() : string
    {
        if (is_callable($this->handler)) {
            return 'closure';
        } else {
            return 'string';
        }
    }

    /**
     * Retrieves the route's methods.
     *
     * @return array
     */
    public function getMethod() : array
    {
        return $this->method;
    }

    /**
     * Retrieves the route's uri.
     *
     * @return string
     */
    public function getUri() : string
    {
        return rtrim(($this->uri[0] !== '/' ? '/' : '').$this->uri, '/');
    }

    /**
     * Retrieves the route's handler.
     *
     * @return \Closure|string
     */
    public function getHandler()
    {
        return $this->handler;
    }
}
