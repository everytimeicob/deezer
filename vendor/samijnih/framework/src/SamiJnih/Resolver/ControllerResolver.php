<?php

namespace SamiJnih\Resolver;

use SamiJnih\Contracts\Resolver\ControllerResolverContract;
use SamiJnih\Exception\ClassNotFound;
use SamiJnih\Exception\ClassMethodNotFound;
use SamiJnih\Http\Request;

class ControllerResolver implements ControllerResolverContract
{
    /**
     * Gets the controller of the request.
     *
     * @param  Request $request
     *
     * @throws ClassNotFound       Thrown if the class does not exist.
     * @throws ClassMethodNotFound Thrown if the class's method does not exist.
     *
     * @return \Closure|array
     */
    public function getController(Request $request)
    {
        $route = $request->attributes->get('route');
        $handler = $route->getHandler();
        $handlerType = $route->getHandlerType();

        if ($handlerType === 'closure') {
            return $handler;
        } else {
            $split = explode('@', $handler);
            $namespace = 'App\\Http\\Controllers\\';
            $controller = $namespace.$split[0];
            $method = $split[1];

            if (!class_exists($controller)) {
                throw new ClassNotFound("Controller {$controller} does not exist.");
            }

            $instance = new $controller();

            if (!method_exists($instance, $method)) {
                throw new ClassMethodNotFound("Method {$method} for controller {$controller} does not exist.");
            }

            return [$instance, $method];
        }
    }
}
