<?php

namespace SamiJnih\Resolver;

use ReflectionClass;
use ReflectionFunction;
use Reflector;
use SamiJnih\Container\Container;
use SamiJnih\Contracts\Resolver\ArgumentResolverContract;
use SamiJnih\Exception\ServiceNotFound;
use SamiJnih\Foundation\Application;
use SamiJnih\Http\Request;

class ArgumentResolver implements ArgumentResolverContract
{
    /**
     * The container instance.
     *
     * @var Container
     */
    protected $container;


    /**
     * Constructor.
     *
     * @param  Container $container
     *
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Gets the controller of the request.
     *
     * @param  Request $request
     * @param  \Closure|array
     *
     * @return array
     */
    public function getArguments(Request $request, $controller) : array
    {
        if ($controller instanceof \Closure) {
            return $this->findArguments($request, new ReflectionFunction($controller));
        } else {
            return $this->findArguments(
                $request,
                (new ReflectionClass($controller[0]))->getMethod($controller[1])
            );
        }
    }

    /**
     * Finds arguments based on the given Reflection.
     *
     * @param  Request   $request
     * @param  Reflector $reflection
     *
     * @throws ServiceNotFound Thrown if the class does not exist.
     *
     * @return array
     */
    protected function findArguments(Request $request, Reflector $reflection) : array
    {
        $arguments = [];

        foreach ($reflection->getParameters() as $p) {
            $pName = $p->getName();

            if (null !== ($pClass = $p->getClass())) {
                $pClass = $pClass->getName();

                if ($pClass === Application::class) {
                    $arguments[] = $this->container;
                } else {
                    $subReflection = new ReflectionClass($pClass);
                    $service = snake_case($subReflection->getShortName());

                    if (isset($this->container[$service])) {
                        $arguments[] = $this->container[$service];
                    } else {
                        throw new ServiceNotFound("The class {$pClass} does not exist in the container. Please ensure that the service has been registered within service providers.");
                    }
                }
            } elseif ($request->attributes->get('parameters')->has($pName)) {
                $arguments[] = $request->attributes->get('parameters')->get($pName);
            }
        }

        return $arguments;
    }
}
