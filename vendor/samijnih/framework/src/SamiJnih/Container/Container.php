<?php

namespace SamiJnih\Container;

class Container implements \ArrayAccess
{
    /**
     * The current instance.
     *
     * @var Container
     */
    protected static $instance;

    /**
     * The values of the application.
     *
     * @var array
     */
    protected $container = [];


    /**
     * Constructor.
     *
     * @param  array|[] $values An array containing values to store.
     *
     * @return void
     */
    public function __construct(array $values = [])
    {
        $this->container = $values;
        static::$instance = $this;
    }

    /**
     * Destructor.
     *
     * @return void
     */
    public function __destruct()
    {
        static::$instance = null;
    }

    /**
     * Sets the current container available everywhere.
     *
     * @return static
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}
