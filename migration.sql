CREATE DATABASE deezer CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE deezer;

CREATE TABLE IF NOT EXISTS `users`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(191) NOT NULL,
    `email` VARCHAR(191) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    CONSTRAINT unique_email UNIQUE (`email`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `bands`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    CONSTRAINT unique_name UNIQUE (`name`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `songs`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `band_id` INT(11) NOT NULL,
    `title` VARCHAR(191) NOT NULL,
    `duration` INT(11) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`band_id`) REFERENCES `bands`(`id`),
    CONSTRAINT unique_song UNIQUE (`title`, `band_id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `user_song`(
    `user_id` INT(11) NOT NULL,
    `song_id` INT(11) NOT NULL,
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`),
    FOREIGN KEY (`song_id`) REFERENCES `songs`(`id`),
    CONSTRAINT unique_song UNIQUE (`user_id`, `song_id`)
) ENGINE = InnoDB;

INSERT INTO `users` SET `username` = "everytimeicob", `email` = "everytimeicob@gmail.com";
INSERT INTO `bands` SET `name` = "Arayana";
INSERT INTO `songs` SET `band_id` = 1, `title` = "Winter Of The Nephilim", `duration` = 164;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Act I: Permafrost (The Cryomancer)", `duration` = 310;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Chizuru No Namida", `duration` = 300;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Act II: A Furiae Named Lorelei (Feat Joel Tock, ex-Dead Silence Hides My Cries)", `duration` = 309;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Chloe Price", `duration` = 289;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Act III: Last Crimson Rain", `duration` = 234;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Morana's Embrace", `duration` = 143;
INSERT INTO `songs` SET `band_id` = 1, `title` = "Aria Of A Thespian", `duration` = 99;
INSERT INTO `songs` SET `band_id` = 1, `title` = "My Ashes: Second Breath", `duration` = 322;
INSERT INTO `songs` SET `band_id` = 1, `title` = "The Alchemist", `duration` = 240;
