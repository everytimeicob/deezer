<?php

namespace App\Service;

use SamiJnih\Database\DB;

class EntityManager
{
    /**
     * The database connection.
     *
     * @var DB
     */
    protected $db;

    /**
     * Collection of repositories
     *
     * @var <RepositoryContract>
     */
    protected $repositories;


    /**
     * Constructor.
     *
     * @param  DB $db
     *
     * @return void
     */
    public function __construct(DB $db)
    {
        $this->db = $db;

        $this->repositories = [
            'User' => new \App\Repository\UserRepository($db),
            'Song' => new \App\Repository\SongRepository($db),
        ];
    }

    /**
     * Retrieves a repository.
     *
     * @param  string $repo
     *
     * @return RepositoryContract|null
     */
    public function getRepository(string $repo)
    {
        if (isset($this->repositories[$repo])) {
            return $this->repositories[$repo];
        }

        return null;
    }
}
