<?php

namespace App\Providers;

use SamiJnih\Contracts\Provider\ServiceProviderContract;
use SamiJnih\Foundation\Application;

class RouteServiceProvider implements ServiceProviderContract
{
    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
        // 
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $router = $app['router'];

        $router->get('users/{userId}', 'UserController@show');
        $router->get('users/{userId}/songs', 'UserController@showSongs');
        $router->post('users/{userId}/songs/{songId}', 'UserController@addSong');
        $router->delete('users/{userId}/songs/{songId}', 'UserController@deleteSong');

        $router->get('songs/{id}', 'SongController@show');
    }
}
