<?php

namespace App\Providers;

use App\Service\EntityManager;
use SamiJnih\Contracts\Provider\ServiceProviderContract;
use SamiJnih\Foundation\Application;

class EntityManagerProvider implements ServiceProviderContract
{
    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
        // 
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $app['entity_manager'] = new EntityManager($app['db']);
    }
}
