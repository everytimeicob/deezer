<?php

namespace App\Providers;

use SamiJnih\Contracts\Provider\ServiceProviderContract;
use SamiJnih\Foundation\Application;

class AppServiceProvider implements ServiceProviderContract
{
    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
        // 
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        //
    }
}
