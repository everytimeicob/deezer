<?php

namespace App\Entity;

use DataTime;

class User
{
    /**
     * User id.
     *
     * @var int
     */
    private $id;

    /**
     * User user name.
     *
     * @var string
     */
    private $username;

    /**
     * User email.
     *
     * @var string
     */
    private $email;

    /**
     * User created at.
     *
     * @var DateTime
     */
    private $created_at;

    /**
     * User updated at.
     *
     * @var DateTime
     */
    private $updated_at;


    /**
     * Constructor.
     *
     * @param  array $data
     *
     * @return void
     */
    public function __construct(array $data = [])
    {
        $this->id = (int) ($data['user_id'] ?? null);
        $this->username = $data['user_username'] ?? null;
        $this->email = $data['user_email'] ?? null;
        $this->created_at = $data['user_created_at'] ?? null;
        $this->updated_at = $data['user_updated_at'] ?? null;
    }

    /**
     * Converts the current object to JSON for JSON response.
     *
     * @return array
     */
    public function toJSON() : array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'username' => $this->username,
        ];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set username.
     *
     * @param  string $username
     *
     * @return $this
     */
    public function setUsername(string $username) : User
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * Set email.
     *
     * @param  string $email
     *
     * @return $this
     */
    public function setEmail(string $email) : User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * Set created at.
     *
     * @param  DateTime|null $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt = null) : User
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get created at.
     *
     * @return DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated at.
     *
     * @param  DateTime|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt = null) : User
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updated at.
     *
     * @return DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
