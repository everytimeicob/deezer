<?php

namespace App\Entity;

use DataTime;

class Song
{
    /**
     * Song id.
     *
     * @var int
     */
    private $id;

    /**
     * Song title.
     *
     * @var string
     */
    private $title;

    /**
     * Song duration.
     *
     * @var int
     */
    private $duration;

    /**
     * Song created at.
     *
     * @var DateTime
     */
    private $created_at;

    /**
     * Song updated at.
     *
     * @var DateTime
     */
    private $updated_at;

    /**
     * Song's band.
     *
     * @var Band
     */
    private $band;


    /**
     * Constructor.
     *
     * @param  array $data
     *
     * @return void
     */
    public function __construct(array $data = [])
    {
        $this->id = (int) ($data['song_id'] ?? null);
        $this->title = $data['song_title'] ?? null;
        $this->duration = (int) ($data['song_duration'] ?? null);
        $this->created_at = $data['song_created_at'] ?? null;
        $this->updated_at = $data['song_updated_at'] ?? null;

        if (isset($data['band']) && $data['band'] instanceof Band) {
            $this->band = $data['band'];
        }
    }

    /**
     * Converts the current object to JSON for JSON response.
     *
     * @return array
     */
    public function toJSON() : array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'duration' => $this->duration,
            'band' => $this->band instanceof Band ? $this->band->toJSON() : $this->band,
        ];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param  string $title
     *
     * @return $this
     */
    public function setTitle(string $title) : Song
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * Set duration.
     *
     * @param  int $duration
     *
     * @return $this
     */
    public function setDuration(int $duration) : Song
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     *
     * @return int
     */
    public function getDuration() : int
    {
        return $this->duration;
    }

    /**
     * Set created at.
     *
     * @param  DateTime|null $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt = null) : Song
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get created at.
     *
     * @return DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated at.
     *
     * @param  DateTime|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt = null) : Song
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updated at.
     *
     * @return DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Returns the song's band.
     *
     * @return Band|null
     */
    public function getBand()
    {
        return $this->band;
    }
}
