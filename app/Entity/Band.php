<?php

namespace App\Entity;

use DataTime;

class Band
{
    /**
     * Band id.
     *
     * @var int
     */
    private $id;

    /**
     * Band name.
     *
     * @var string
     */
    private $name;

    /**
     * Band created at.
     *
     * @var DateTime
     */
    private $created_at;

    /**
     * Band updated at.
     *
     * @var DateTime
     */
    private $updated_at;


    /**
     * Constructor.
     *
     * @param  array $data
     *
     * @return void
     */
    public function __construct(array $data = [])
    {
        $this->id = (int) ($data['band_id'] ?? null);
        $this->name = $data['band_name'] ?? null;
        $this->created_at = $data['band_created_at'] ?? null;
        $this->updated_at = $data['band_updated_at'] ?? null;
    }

    /**
     * Converts the current object to JSON for JSON response.
     *
     * @return array
     */
    public function toJSON() : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param  string $name
     *
     * @return $this
     */
    public function setName(string $name) : Band
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Set created at.
     *
     * @param  DateTime|null $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt = null) : Band
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get created at.
     *
     * @return DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated at.
     *
     * @param  DateTime|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt = null) : Song
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updated at.
     *
     * @return DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
