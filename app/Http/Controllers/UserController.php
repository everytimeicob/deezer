<?php

namespace App\Http\Controllers;

use App\Service\EntityManager;
use SamiJnih\Http\JsonResponse;

class UserController
{
    /**
     * Returns the information of a given user.
     *
     * @param  EntityManager $em
     * @param  string        $userId
     *
     * @return JsonResponse
     */
    public function show(EntityManager $em, $userId) : JsonResponse
    {
        $code = 404;
        $response = ['status' => ['message' => 'User not found.', 'code' => $code], 'result' => null];

        if ($user = $em->getRepository('User')->find($userId)) {
            $code = 200;
            $response['status']['message'] = 'success';
            $response['status']['code'] = $code;
            $response['result'] = $user->toJSON();
        }

        return new JsonResponse($response, $code);
    }

    /**
     * Returns all the given user's favorite songs.
     *
     * @param  EntityManager $em
     * @param  string        $userId
     *
     * @return JsonResponse
     */
    public function showSongs(EntityManager $em, $userId) : JsonResponse
    {
        $code = 404;
        $response = ['status' => ['message' => 'User not found.', 'code' => $code], 'result' => null];

        $repo = $em->getRepository('User');

        if ($repo->exists($userId)) {
            $songs = $repo->findWithFavoriteSongs($userId);

            $code = 200;
            $response['status']['message'] = 'success';
            $response['status']['code'] = $code;
            $response['result'] = $songs;
        }

        return new JsonResponse($response, $code);
    }

    /**
     * Adds a new song to a given user's favorite.
     *
     * @param  EntityManager $em
     * @param  string        $userId
     * @param  string        $songId
     *
     * @return JsonResponse
     */
    public function addSong(EntityManager $em, $userId, $songId) : JsonResponse
    {
        $code = 404;
        $response = ['status' => ['message' => 'User not found.', 'code' => $code], 'result' => null];

        $repo = $em->getRepository('User');

        if ($repo->exists($userId)) {
            if ($repo->addSongToFavorite($userId, $songId)) {
                $code = 201;
                $response = null;
            } else {
                $code = 500;
                $response['status']['message'] = 'The song could not be added.';
                $response['status']['code'] = $code;
            }
        }

        return new JsonResponse($response, $code);
    }

    /**
     * Removes a song from a given user's favorite.
     *
     * @param  EntityManager $em
     * @param  string        $userId
     * @param  string        $songId
     *
     * @return JsonResponse
     */
    public function deleteSong(EntityManager $em, $userId, $songId) : JsonResponse
    {
        $code = 404;
        $response = ['status' => ['message' => 'User not found.', 'code' => $code], 'result' => null];

        $repo = $em->getRepository('User');

        if ($repo->exists($userId)) {
            if ($repo->removeSongFromFavorite($userId, $songId)) {
                $code = 204;
                $response = null;
            } else {
                $code = 500;
                $response['status']['message'] = 'The song could not be removed.';
                $response['status']['code'] = $code;
            }
        }

        return new JsonResponse($response, $code);
    }
}
