<?php

namespace App\Http\Controllers;

use App\Service\EntityManager;
use SamiJnih\Http\JsonResponse;

class SongController
{
    /**
     * Returns the information of a given song.
     *
     * @param  EntityManager $em
     * @param  string        $id
     *
     * @return JsonResponse
     */
    public function show(EntityManager $em, $id)
    {
        $code = 404;
        $response = ['status' => ['message' => 'Song not found.', 'code' => $code], 'result' => null];

        if ($song = $em->getRepository('Song')->find($id)) {
            $code = 200;
            $response['status']['message'] = 'success';
            $response['status']['code'] = $code;
            $response['result'] = $song->toJSON();
        }

        return new JsonResponse($response, $code);
    }
}
