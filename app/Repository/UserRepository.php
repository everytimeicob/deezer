<?php

namespace App\Repository;

use App\Entity\Band;
use App\Entity\Song;
use App\Entity\User;
use PDO;
use SamiJnih\Repository\Repository;

class UserRepository extends Repository
{
    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $stmt = $this->db->prepare('SELECT users.id AS user_id, users.username AS user_username, users.email AS user_email FROM users WHERE users.id = :id');

        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        if (false !== ($result = $stmt->fetch(PDO::FETCH_ASSOC))) {
            return new User($result);
        } else {
            return null;
        }
    }

    /**
     * Checks if the given user exist.
     *
     * @param  string|int $id
     *
     * @return bool
     */
    public function exists($id) : bool
    {
        $stmt = $this->db->prepare('SELECT COUNT(users.id) FROM users WHERE users.id = :id');

        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return (bool) $stmt->fetchColumn();
    }

    /**
     * Retrieves an user with his favorite songs.
     *
     * @param  string|int $id
     *
     * @return array
     */
    public function findWithFavoriteSongs($id) : array
    {
        $stmt = $this->db->prepare('SELECT songs.id AS song_id, songs.title AS song_title, songs.duration AS song_duration, bands.id AS band_id, bands.name AS band_name FROM users INNER JOIN user_song ON user_song.user_id = users.id INNER JOIN songs ON songs.id = user_song.song_id INNER JOIN bands ON bands.id = songs.band_id WHERE users.id = :id');

        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $songs = [];

        foreach (($results = $stmt->fetchAll(PDO::FETCH_ASSOC)) as &$row) {
            $row['band'] = new Band($row);
            $songs[] = (new Song($row))->toJSON();
        }

        unset($row);

        return $songs;
    }

    /**
     * Flags a song to a given user's favorite.
     *
     * @param  string|int $userId
     * @param  string|int $songId
     *
     * @return bool
     */
    public function addSongToFavorite($userId, $songId) : bool
    {
        $stmt = $this->db->prepare('INSERT INTO user_song SET user_song.user_id = :user_id, user_song.song_id = :song_id');
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $stmt->bindValue(':song_id', $songId, PDO::PARAM_INT);

        return $stmt->execute();
    }

    /**
     * Unflags a given song as favorite of a given user.
     *
     * @param  string|int $userId
     * @param  string|int $songId
     *
     * @return bool
     */
    public function removeSongFromFavorite($userId, $songId) : bool
    {
        $stmt = $this->db->prepare('DELETE FROM user_song WHERE user_song.user_id = :user_id AND user_song.song_id = :song_id');

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $stmt->bindValue(':song_id', $songId, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->rowCount() > 0;
    }
}
