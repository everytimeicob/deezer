<?php

namespace App\Repository;

use App\Entity\Band;
use App\Entity\Song;
use PDO;
use SamiJnih\Repository\Repository;

class SongRepository extends Repository
{
    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $stmt = $this->db->prepare('SELECT songs.id AS song_id, songs.title AS song_title, songs.duration AS song_duration, bands.id AS band_id, bands.name AS band_name FROM songs INNER JOIN bands ON bands.id = songs.band_id WHERE songs.id = :id');

        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        if (false !== ($result = $stmt->fetch(PDO::FETCH_ASSOC))) {
            $result['band'] = new Band($result);

            return new Song($result);
        } else {
            return null;
        }
    }
}
