## Message pour l'équipe de recrutement de DEEZER

Je tiens à vous remercier de m'avoir permis de passer ce test technique. Il était vraiment plaisant et m'a obligé à sortir de ma "zone de confort" en utilisant de façon "bête et méchante" un framework qui pouvait me fournir clé en main, tous les outils facilitant le développement.

J'ai respecté la consigne, pas de librairies ni de frameworks. J'ai développé mon propre micro-framework à partir de mes connaissances en Laravel/Silex, sur leurs utilisations ainsi que sur leurs fonctionnement en interne, en ayant déjà été plusieurs fois lire leurs codes sources dans le passé.

Vous trouverez donc un micro-framework standalone situé dans le `vendor`, que j'utilise dans l'application à la racine à partir des dossiers `bootstrap` et `app`.

Le schéma de base de données est `migration.php`

### Pré-requis

* PHP >=7.0.0
* un serveur web si vous voulez le lancer et le tester (à la base je travaillais dessus avec DOCKER et nginx, mais j'ai eu pas mal de problème avec PDO alors j'ai dû basculer de DOCKER à MAMP.. :( )
* un virtual host `dev.deezer.fr` mais le localhost marche aussi
* un utilisateur appelé `deezer` pour la base de données `deezer` qui sera créée par la migration, avec le mot de passe `T6ynzKwKF87XT7v2`

### Info

* Les informations de connexion à la BDD sont éditable depuis `configuration.php`
* Aucun copié collé n'a été effectué afin de développer le micro-framework du vendor
* J'ai pris 4 jours pour le développer, j'espère ne pas être pénalisé d'avoir développé mon propre micro-framework pour faciliter le développement de l'API...
* Je n'ai pas réalisé l'exercice 2 car :
    * facultatif
    * j'ai passé 4 jours sur l'exercice 1 comme dit plus haut
    * je suis en vacance à compter de lundi et c'était planifié depuis pas mal de mois, du coup pas d'ordi

### Routes Users
* GET `users/{userId}`
* GET `users/{userId}/songs`
* POST `users/{userId}/songs/{songId}`
* DELETE `users/{userId}/songs/{songId}`

### Route Songs
* GET `songs/{id}`