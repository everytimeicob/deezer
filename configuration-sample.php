<?php

define('APP_ENV', 'local');
define('APP_URL', 'http://localhost');
define('DB_CONNECTION', 'mysql');
define('DB_HOST', 'localhost');
// define('DB_SOCKET', '/var/run/mysqld/mysqld.sock');
define('DB_PORT', '3306');
define('DB_DATABASE', 'forge');
define('DB_USERNAME', 'forge');
define('DB_PASSWORD', '');
